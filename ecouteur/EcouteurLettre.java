package boggle.ecouteur;

import boggle.model.Boggle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class EcouteurLettre implements EventHandler<ActionEvent> {
    private Boggle boggle;
    private int lig;
    private int col;

    public EcouteurLettre(Boggle boggle, int lig, int col) {
        this.boggle = boggle;
        this.lig = lig;
        this.col = col;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        this.boggle.ajouterLettre(this.lig, this.col);
    }
}
