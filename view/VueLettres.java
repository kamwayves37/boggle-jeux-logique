package boggle.view;

import boggle.ecouteur.EcouteurLettre;
import boggle.model.Boggle;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class VueLettres extends GridPane implements Vue {
    private Boggle boggle;

    public VueLettres(Boggle boggle) {
        super();

        this.boggle = boggle;
        this.boggle.ajouterVue(this);

        for (int i = 0; i < this.boggle.size(); i++) {
            for (int j = 0; j < boggle.size(); j++) {
                Button bouton = new Button(String.valueOf(this.boggle.getLettre(i, j)));
                bouton.setOnAction(new EcouteurLettre(boggle, i, j));
                bouton.setMinHeight(80);
                bouton.setMinWidth(80);

                this.add(bouton, i,j);
            }
        }

        this.setStyle("-fx-padding: 10; " +
                      "-fx-alignment: center;" +
                      "-fx-font: 14 arial;");

    }

    @Override
    public void mettreAJour() {

    }
}
