package boggle.view;

import boggle.ecouteur.EcouteurEffacer;
import boggle.ecouteur.EcouteurQuitter;
import boggle.ecouteur.EcouteurValider;
import boggle.model.Boggle;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class PanneauControle extends VBox implements Vue {
    private Button valider;
    private Button effacer;
    private Button quitter;
    private Boggle boggle;

    public PanneauControle(Boggle boggle){
        this.boggle = boggle;

        this.valider = new Button("Valider");
        this.effacer = new Button("Effacer");
        this.quitter = new Button("Quitter");
        this.getChildren().addAll(this.valider, this.effacer, this.quitter);

        this.setStyle("-fx-padding: 10; " +
                      "-fx-spacing: 8;" +
                      "-fx-alignment: center;" +
                      "-fx-font: 14 arial;");

        this.valider.setOnAction(new EcouteurValider(this.boggle));
        this.effacer.setOnAction(new EcouteurEffacer(this.boggle));
        this.quitter.setOnAction(new EcouteurQuitter(this.boggle));
    }

    @Override
    public void mettreAJour() {
    }
}
