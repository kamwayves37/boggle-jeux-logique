package boggle.view;

import boggle.model.Boggle;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class MenuControle extends MenuBar implements Vue{
    private Boggle boggle;

    public MenuControle(Boggle boggle) {
        this.boggle = boggle;

        Menu menu = new Menu("BOGGLE");
        MenuItem menuSauv = new MenuItem("Sauvegarder");
        MenuItem menuRep = new MenuItem("Reprendre");

        menu.getItems().add(menuSauv);
        menu.getItems().add(menuRep);

        this.getMenus().add(menu);
    }

    @Override
    public void mettreAJour() {}
}
