package boggle.view;

import boggle.model.Boggle;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.TilePane;

public class VueInfos extends TilePane implements Vue {
    private Label score, motChoisi;
    private Boggle boggle;

    public VueInfos(Boggle boggle) {
        super();
        this.boggle = boggle;
        this.boggle.ajouterVue(this);

        motChoisi = new Label("MOT CHOISI " + this.boggle.getMotChoisi());
        score = new Label("SCORE " + this.boggle.getScore());

        this.getChildren().add(motChoisi);
        this.getChildren().add(score);

        this.setStyle("-fx-padding: 10; " +
                "-fx-spacing: 8;" +
                "-fx-alignment: center;" +
                "-fx-font: 14 arial;");
    }

    @Override
    public void mettreAJour() {
        this.score.setText("SCORE: " +this.boggle.getScore());
        this.motChoisi.setText("MOT CHOISI: " + this.boggle.getMotChoisi());
    }
}
