package boggle;

import boggle.model.Boggle;
import boggle.view.MenuControle;
import boggle.view.PanneauControle;
import boggle.view.VueInfos;
import boggle.view.VueLettres;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Boggle bog = new Boggle(4);
        BorderPane root = new BorderPane();
        root.setTop(new MenuControle(bog));
        root.setBottom(new VueInfos(bog));
        root.setCenter(new VueLettres(bog));
        root.setRight(new PanneauControle(bog));
        primaryStage.setScene(new Scene(root, 700, 600));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
